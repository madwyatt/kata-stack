package desck.katastack;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class KataStackApplicationTests {

    KataStackApplication stackApplication;

    @Before
    public void setUp() {
        stackApplication = new KataStackApplication();
    }

    @Test
    public void testAssertNotNull() {
        assertNotNull("should not be null", stackApplication);
    }

}
